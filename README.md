### About

This program was developed during my last year at college as a final project for the Information Systems Security module.

It adds a nice twist to the [Steganography](https://en.wikipedia.org/wiki/Steganography) technique by making it follow a unique pattern in the image: that of a maze.

The encoded message can only be retrieved by using the original maze as a map, making it essentially impossible to decode a message without it.

### A map?

The program generates mazes which are used as 'keys' to the encryption process. All generated mazes are images which have a black maze drawn on a white background, with a red dot marking where the encryption process starts.

### Encoding

The encoding process consists on travelling the maze in a fixed pattern, while altering the image color's pixels on its path to encode the ASCII representation of the text to be encoded. Only the last bit of each color byte is altered, which makes it very hard to identify an 'encrypted' image, unless the image itself is completely monochromatic or contains large monochromatic patchs (large areas of the image where the color is exactly the same).

### Decoding

The decoding process works essentially the same way, travelling the maze in the same fixed pattern, while reading the last bit of the image color's pixels on its path to build the bytes contaning the ASCII representation of the text to be encoded.
