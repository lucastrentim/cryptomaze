<?php

namespace LucasTrentim\CryptomazeBundle\Controller;

use LucasTrentim\CryptomazeBundle\Entity\Cryptomaze;
use LucasTrentim\CryptomazeBundle\Entity\Maze;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Length;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CryptomazeBundle:Default:index.html.twig');
    }

    public function encodeAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add(
                'text',
                TextareaType::class,
                [
                    'constraints' => new Length(['min' => 3]),
                ]
            )
            ->add('image', FileType::class, [
                "label" => "Image"
            ])
            ->add('map', FileType::class, [
                "label"    => "Map",
                "required" => false
            ])
            ->add('send', SubmitType::class, ['label' => 'Encode'])
            ->getForm();

        $form->handleRequest($request);

        $encodedInfo = null;
        $error = null;

        try {
            if ($form->isValid()) {
                $data = $form->getData();

                $cryptoMaze = Cryptomaze::createInstanceFromData($data);
                $cryptoMaze->encode();

                $encodedInfo = [
                    "map"   => base64_encode($cryptoMaze->getMapPng()),
                    "image" => base64_encode($cryptoMaze->getImagePng()),
                ];
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        $twigParameters = [
            "error"       => $error,
            "form"        => $form->createView(),
            "encodedInfo" => $encodedInfo
        ];

        return $this->render('CryptomazeBundle:Default:encode.html.twig', $twigParameters);
    }

    public function generateMazeAction(Request $request)
    {
        /* Defaults to 100x100 maze */
        $width = $request->get("width", 100);
        $height = $request->get("height", 100);

        $image = Maze::generateMazeImage($width, $height);
        $content = Cryptomaze::imageToPng($image);

        /* These headers will force a download response from this action */
        $headers = [
            'Content-Type'              => 'image/png',
            'Content-Description'       => 'File Transfer',
            'Content-Transfer-Encoding' => 'binary',
            'Connection'                => 'Keep-Alive',
            'Expires'                   => '0',
            'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'                    => 'public',
            'Content-Disposition'       => 'attachment; filename=maze.png',
        ];

        return new Response($content, 200, $headers);
    }

    public function decodeAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('image', FileType::class, ["label" => "Image"])
            ->add('map', FileType::class, ["label" => "Map"])
            ->add('send', SubmitType::class, ['label' => 'Decode'])
            ->getForm();

        $form->handleRequest($request);

        $decodedText = null;
        $error = null;

        try {
            if ($form->isValid()) {
                $data = $form->getData();

                $cryptoMaze = Cryptomaze::createInstanceFromData($data, false);
                $cryptoMaze->decode();

                $decodedText = $cryptoMaze->getDecodedText();
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        $twigParameters = [
            "error"       => $error,
            "form"        => $form->createView(),
            "decodedText" => $decodedText
        ];

        return $this->render('CryptomazeBundle:Default:decode.html.twig', $twigParameters);
    }

    public function contactAction()
    {
        return $this->render('CryptomazeBundle:Default:contact.html.twig');
    }
}
