<?php

namespace LucasTrentim\CryptomazeBundle\Entity;

/**
 * Class Point
 *
 * Represents a point (or pixel) within the map image.
 * Stores information about color representation
 *
 * @package LucasTrentim\CryptomazeBundle\Entity
 */
class Point
{
    /**
     * Map's encoded information color in Hexadecimal notation
     */
    const COLOR_TRACKED = "00FF00";
    protected static $trackedColor = null;

    /**
     * Map's origin color in Hexadecimal notation
     */
    const COLOR_ORIGIN = "FF0000";
    protected static $originColor = null;

    /**
     * Map's path color in Hexadecimal notation
     */
    const COLOR_PATH = "000000";
    protected static $pathColor = null;

    /**
     * Map's wall color in Hexadecimal notation
     */
    const COLOR_EMPTY = "FFFFFF";
    protected static $emptyColor = null;

    /**
     * Returns a integer representation of the color which represents the where information has been encoded in a maze
     * @return integer
     */
    public static function getTrackedColor()
    {
        if (self::$trackedColor === null) {
            self::$trackedColor = hexdec(self::COLOR_TRACKED);
        }

        return self::$trackedColor;
    }

    /**
     * Returns a integer representation of the color which represents the maze's start point
     * @return integer
     */
    public static function getOriginColor()
    {
        if (self::$originColor === null) {
            self::$originColor = hexdec(self::COLOR_ORIGIN);
        }

        return self::$originColor;
    }

    /**
     * Returns a integer representation of the color which represents corridors in a maze
     * @return integer
     */
    public static function getPathColor()
    {
        if (self::$pathColor === null) {
            self::$pathColor = hexdec(self::COLOR_PATH);
        }

        return self::$pathColor;
    }

    /**
     * Returns a integer representation of the color which represents walls in a maze
     * @return integer
     */
    public static function getEmptyColor()
    {
        if (self::$emptyColor === null) {
            self::$emptyColor = hexdec(self::COLOR_EMPTY);
        }

        return self::$emptyColor;
    }

    /**
     * X coordinate
     * @var integer
     */
    public $x;
    /**
     * Y coordinate
     * @var integer
     */
    public $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Locates and returns map start point
     * @param Resource $image
     * @return Point
     */
    public static function locateStartPoint($image)
    {
        $originColor = self::getOriginColor();

        for ($x = 0; $x < imagesx($image); $x++) {
            for ($y = 0; $y < imagesy($image); $y++) {
                $color = imagecolorat($image, $x, $y);
                if ($color == $originColor) {
                    return new Point($x, $y);
                }
            }
        }

        return null;
    }

    /**
     * Locates free adjascent points for maze generation.
     * @param Resource $image
     * @return Point[]
     */
    public function getMazeGrowthNodes($image)
    {
        return $this->getAdjascent($image, 2, self::getEmptyColor());
    }

    /**
     * Finds and returns the possible walkable paths from this point
     * @param Resource $image
     * @param bool $isEncoding
     * @return Point[]
     */
    public function possiblePathways($image, $isEncoding = true)
    {
        return $this->getAdjascent($image, 1, $isEncoding ? self::getPathColor() : self::getTrackedColor());
    }

    /**
     * Finds and returns the possible walkable paths from this point
     * @param Resource $image
     * @param integer $distance
     * @param string $pathwayColor
     * @return Point[]
     */
    public function getAdjascent($image, $distance, $pathwayColor)
    {
        $adjascentes = [];
        $x = $this->x;
        $y = $this->y;

        if ($y - $distance > 0 && imagecolorat($image, $x, $y - $distance) == $pathwayColor) {
            $adjascentes[] = new Point($x, $y - $distance);
        }

        if ($x + $distance < imagesx($image) && imagecolorat($image, $x + $distance, $y) == $pathwayColor) {
            $adjascentes[] = new Point($x + $distance, $y);
        }

        if ($y + $distance < imagesy($image) && imagecolorat($image, $x, $y + $distance) == $pathwayColor) {
            $adjascentes[] = new Point($x, $y + $distance);
        }

        if ($x - $distance > 0 && imagecolorat($image, $x - $distance, $y) == $pathwayColor) {
            $adjascentes[] = new Point($x - $distance, $y);
        }

        return $adjascentes;
    }

    /**
     * Returns a random point on valid pathways within bounds.
     *
     * @param integer $width
     * @param integer $height
     * @return Point
     */
    public static function getRandomStartPoint($width, $height)
    {
        return new Point((rand(0, ($width - 2) / 2) * 2) + 1, (rand(0, ($height - 2) / 2) * 2) + 1);
    }
}
