<?php

namespace LucasTrentim\CryptomazeBundle\Entity;

/**
 * Class Maze
 *
 * Responsible for creating the maze images
 *
 * @package LucasTrentim\CryptomazeBundle\Entity
 */
class Maze
{
    /**
     * Creates a maze of the $width and $height specified
     *
     * @param integer $width
     * @param integer $height
     * @return resource
     */
    public static function generateMazeImage($width = 100, $height = 100)
    {
        $image = imagecreatetruecolor($width, $height);

        imagefilledrectangle($image, 0, 0, $width - 1, $height - 1, Point::getEmptyColor());

        $firstPoint = Point::getRandomStartPoint($width, $height);

        $nextPoint = $firstPoint;

        $forkPoints = [];

        do {
            $currentPoint = $nextPoint;
            $nextPoint = null;

            if ($nodes = $currentPoint->getMazeGrowthNodes($image)) {
                if (count($nodes) > 1 && !in_array($currentPoint, $forkPoints)) {
                    $forkPoints[] = $currentPoint;
                }

                $nextPoint = $nodes[array_rand($nodes)];
                imageline(
                    $image,
                    $currentPoint->x, $currentPoint->y,
                    $nextPoint->x, $nextPoint->y,
                    Point::getPathColor()
                );
            } else {
                $nextPoint = array_pop($forkPoints);
            }
        } while ($nextPoint);

        imagesetpixel($image, $firstPoint->x, $firstPoint->y, Point::getOriginColor());

        return $image;
    }
}
