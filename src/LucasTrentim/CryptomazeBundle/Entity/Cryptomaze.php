<?php

namespace LucasTrentim\CryptomazeBundle\Entity;


use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Cryptomaze
 *
 * Encodes and Decodes information on/from images using a map.
 *
 * @package LucasTrentim\CryptomazeBundle\Entity
 */
class Cryptomaze
{
    /**
     * Contains the map containing the route where the information should be encoded on
     * @var resource
     */
    protected $map;
    /**
     * Contains the image where the information should be encoded in/ decoded from
     * @var resource
     */
    protected $image;
    /**
     * Contains the message to be encoded
     * @var string
     */
    protected $text;
    /**
     * Contains a binary representation of the protected $text property
     * @var string
     */
    protected $binaryText;
    /**
     * Keeps track of which bit of $binaryText is being written
     * @var int
     */
    protected $textPointer = 0;

    /**
     * Cryptomaze constructor.
     * @param resource $map
     * @param resource $image
     * @param string $text
     */
    public function __construct($map, $image, $text = null)
    {
        $this->map = $map;
        $this->image = $image;

        $this->text = $text and $this->binaryText = self::text2bin($text);
    }

    /**
     * Encrypts the message in the image using the maze map
     */
    public function encode()
    {
        $startPointExists = false;
        $finishedEncrypting = false;

        /* @var $points Point[] */
        if ($startPoint = Point::locateStartPoint($this->map)) {

            $points[] = $startPoint;

            while (!empty($points) && !$finishedEncrypting) {
                /* @var $currentPoint Point */
                $currentPoint = array_pop($points);
                $points = array_merge($points, $currentPoint->possiblePathways($this->map));

                if ($startPointExists) {
                    imagesetpixel($this->map, $currentPoint->x, $currentPoint->y, Point::getTrackedColor());
                } else {
                    $startPointExists = imagesetpixel($this->map, $currentPoint->x, $currentPoint->y,
                        Point::getOriginColor());
                }

                $pointColor = imagecolorat($this->image, $currentPoint->x, $currentPoint->y);
                $binaryColor = $this->colorToBin($pointColor);

                for ($i = 1; $i < 4 && !$finishedEncrypting; $i++) {
                    $nextBit = $this->nextTextBit();

                    if ($nextBit === null) {
                        $finishedEncrypting = true;
                    } else {
                        $binaryColor[($i * 8) - 1] = $nextBit;
                    }
                }

                imagesetpixel($this->image, $currentPoint->x, $currentPoint->y, bindec($binaryColor));
            }
        }
    }

    /**
     * Recovers a message from the $image property, using the $map as a guide
     */
    public function decode()
    {
        $this->binaryText = "";

        $points[] = Point::locateStartPoint($this->map);

        while (!empty($points)) {
            /* @var $currentPoint Point */
            $currentPoint = array_pop($points);
            $points = array_merge($points, $currentPoint->possiblePathways($this->map, false));

            imagesetpixel($this->map, $currentPoint->x, $currentPoint->y, Point::getPathColor());


            $pointColor = imagecolorat($this->image, $currentPoint->x, $currentPoint->y);
            $binaryColor = $this->colorToBin($pointColor);

            $this->binaryText .= $binaryColor[7];
            $this->binaryText .= $binaryColor[15];
            $this->binaryText .= $binaryColor[23];
        }
    }

    /**
     * Transforms a color numerical value into its binary representation
     * @param $color
     * @return string
     */
    protected function colorToBin($color)
    {
        return str_pad(decbin($color), 24, "0", STR_PAD_LEFT);
    }

    /**
     * Transforms a string into a ASCII binary representation of itself
     * @param string $text
     * @return string
     */
    protected function text2bin($text)
    {
        $result = null;

        for ($i = 0; $i < strlen($text); $i++) {
            $result .= str_pad(decbin(ord($text[$i])), 8, 0, STR_PAD_LEFT);
        }

        return $result;
    }

    /**
     * Transforms a binary string into a human readable text representation of itself
     * @param string $number
     * @return string
     */
    protected function bin2text($number)
    {
        $result = null;

        $bits = explode("|", chunk_split($number, 8, "|"));

        foreach ($bits as $bit) {
            $result .= chr(bindec($bit));
        }

        return $result;
    }

    /**
     * Moves the textPointer forward in the $binaryText string
     * @staticvar int $indice
     * @return null|int
     */
    protected function nextTextBit()
    {
        return $this->textPointer < strlen($this->binaryText) ? $this->binaryText[$this->textPointer++] : null;

    }

    /**
     * Returns the map image resource
     * @return resource|null
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Returns the image resource
     * @return resource|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the text to be encoded
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Converts instance's binaryText back to its human readable form
     * @return string|null
     */
    public function getDecodedText()
    {
        return $this->bin2text($this->binaryText);
    }

    /**
     * Converts instance's map into a PNG blob
     * @return null|string
     */
    public function getMapPng()
    {
        return self::imageToPng($this->map);
    }

    /**
     * Converts instance's image into a PNG blob
     * @return null|string
     */
    public function getImagePng()
    {
        return self::imageToPng($this->image);
    }

    /**
     * Converts a image resource to a PNG blob
     * @param $image
     * @return null|string
     */
    public static function imageToPng($image)
    {
        $result = null;

        if ($image != null) {
            /* Captures output in order to imbue it in the response */
            ob_start();
            imagepng($image);
            $result = ob_get_contents();
            ob_end_clean();
        }

        return $result;
    }

    /**
     * Creates a Cryptomaze instance from the data array returned from the encryption form
     * @param array $data
     * @param bool $generateMap
     * @return Cryptomaze
     * @throws \Exception
     */
    public static function createInstanceFromData($data, $generateMap = true)
    {
        $map = null;

        /* @var $data UploadedFile[] */
        if ($data["map"] and $data["map"]->isFile() and $fileContents = file_get_contents($data["map"]->getPathname())) {
            $map = imagecreatefromstring($fileContents);
        }

        if (!$map) {
            if ($generateMap) {
                $map = Maze::generateMazeImage();
            } else {
                throw new \Exception("Map is missing or corrupted. Please try again.");
            }
        }

        $image = null;

        /* @var $data UploadedFile[] */
        if ($data["image"] and $data["image"]->isFile() and $fileContents = file_get_contents($data["image"]->getPathname())) {
            $image = imagecreatefromstring($fileContents);
        } else {
            throw new \Exception("Image is missing or corrupted. Please try again.");
        }

        return key_exists("text", $data) ? new Cryptomaze($map, $image, $data["text"]) : new Cryptomaze($map, $image);
    }
}
